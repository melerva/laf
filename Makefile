LUA_VERSION := 5.4

CC ?= cc
CFLAGS += -std=c11
CPPFLAGS += -Isrc -I.
LDFLAGS += -s
LDLIBS += -lphysfs
PKG_CONFIG ?= pkg-config

CFLAGS += $(shell $(PKG_CONFIG) lua$(LUA_VERSION) --cflags-only-other)
CPPFLAGS += $(shell $(PKG_CONFIG) lua$(LUA_VERSION) --cflags-only-I)
LDFLAGS += $(shell $(PKG_CONFIG) lua$(LUA_VERSION) --libs-only-L)
LDFLAGS += $(shell $(PKG_CONFIG) lua$(LUA_VERSION) --libs-only-other)
LDLIBS += $(shell $(PKG_CONFIG) lua$(LUA_VERSION) --libs-only-l)

laf_objs := \
  src/main.o \
  src/modules/laf.o \
  src/modules/mod_gc.o \
  src/packages.o \
  src/require.o

laf:

%.o: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@

laf.info: doc/manual.texi
	makeinfo doc/manual.texi

laf: $(laf_objs)
	$(CC) $(CFLAGS) $(LDFLAGS) -o laf $(laf_objs) $(LDLIBS)

.SECONDARY: $(laf_objs)
