#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <stdbool.h>

int luaopen_mod_gc(lua_State *const L) {
    // [1] = preloader
    // [2] = physfs (inserted in src/require.c)
    // [3] = lua
    // [4] = native
    // [5] = all-in-one
    luaL_loadstring(L,
                    "do\n"
                    "  local pl4 = package.searchers[4]\n"
                    "\n"
                    "  package.searchers[4] = function(modname)\n"
                    "    local r, n = pl4(modname)\n"
                    "\n"
                    "    if (r == nil) or (type(r) == 'string') then\n"
                    "      return r\n"
                    "    else\n"
                    "      local f = function(v)\n"
                    "        local m = r(v)\n"
                    "        package.nogc[modname] = m\n"
                    "        return m\n"
                    "      end\n"
                    "\n"
                    "      return f, v\n"
                    "    end\n"
                    "  end\n"
                    "end\n"

                    "do\n"
                    "  local pl5 = package.searchers[5]\n"
                    "\n"
                    "  package.searchers[5] = function(modname)\n"
                    "    local r, n = pl5(modname)\n"
                    "\n"
                    "    if (r == nil) or (type(r) == 'string') then\n"
                    "      return r\n"
                    "    else\n"
                    "      local f = function(v)\n"
                    "        local m = r(v)\n"
                    "        package.nogc[modname] = m\n"
                    "        return m\n"
                    "      end\n"
                    "\n"
                    "      return f, v\n"
                    "    end\n"
                    "  end\n"
                    "end\n"

                    /* If a package is loaded at this point,
                     * it is probably built-in. */
                    "for n,v in pairs(package.loaded) do\n"
                    "  package.nogc[n] = v\n"
                    "end\n"
    );
    lua_call(L, 0, 0);

    /* make `package.loaded` weak */
     lua_getglobal(L, "package");
      lua_getfield(L, -1, "loaded");
       lua_createtable(L, 0, 1);
        lua_pushstring(L, "v");
       lua_setfield(L, -2, "__mode");
      lua_setmetatable(L, -2);
    lua_pop(L, 2);

    /* this is a "feral" module, return true */
     lua_pushboolean(L, true);
    return 1;
}
