#include <assert.h>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <physfs.h>
#include <stdbool.h>
#include <stdio.h>

struct reader_data {
    char buffer[LUAL_BUFFERSIZE];
    PHYSFS_file *file;
};

static const char *reader(lua_State * L, void *data, size_t * size) {
    struct reader_data *rd = (struct reader_data *)data;
    PHYSFS_sint64 ret = PHYSFS_readBytes(rd->file, rd->buffer, BUFSIZ);

    if (ret == -1)
        luaL_error(L, "error reading file: %s",
                   PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode()));
    *size = (size_t) ret;
    return rd->buffer;
}

static int laf_open(lua_State *const L) {
    static const char *modes[] = { "rb", "wb", "ab", NULL };
    enum {READ, WRITE, APPEND};

    const char *filename = luaL_checkstring(L, 1);
    const int mode = luaL_checkoption(L, 2, "rb", modes);

    PHYSFS_file *file;

    if (mode == READ)
        file = PHYSFS_openRead(filename);
    else if (mode == WRITE)
        file = PHYSFS_openWrite(filename);
    else if (mode == APPEND)
        file = PHYSFS_openAppend(filename);
    else
        assert(0);

    if (file) {
        *(PHYSFS_file **) lua_newuserdatauv(L, sizeof(file), 0) = file;
        luaL_getmetatable(L, "laf.file");
        lua_setmetatable(L, -2);
        return 1;
    } else {
        lua_pushnil(L);
        lua_pushstring(L, PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode()));
        return 2;
    }
}

/* used in src/main.c */
int laf_loadfile(lua_State *const L) {
    struct reader_data rd;
    const char *filename = luaL_checkstring(L, 1);

    rd.file = PHYSFS_openRead(filename);

    if (rd.file == NULL)
        return luaL_error(L, "couldn't open file: '%s'", filename);

    if (lua_load(L, reader, &rd, filename, NULL))
        return lua_error(L);

    PHYSFS_close(rd.file);
    return 1;
}

static PHYSFS_file **check_laf_file(lua_State *const L, const int index) {
    return (PHYSFS_file **)luaL_checkudata(L, index, "laf.file");
}

static PHYSFS_file **check_laf_open_file(lua_State * L, int index) {
    PHYSFS_file **const file = check_laf_file(L, index);

    if (!*file)
        luaL_error(L, "attempt to use a closed file");
    return file;
}

static int laf_file_close(lua_State *const L) {
    PHYSFS_file **const file = check_laf_open_file(L, 1);

    if (PHYSFS_close(*file)) {
        *file = NULL;
        lua_pushboolean(L, 1);
        return 1;
    } else {
        lua_pushnil(L);
        lua_pushstring(L, PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode()));
        return 2;
    }
}

static int laf_file_gc(lua_State *const L) {
    PHYSFS_file *const *const file = check_laf_file(L, 1);
    if (*file)
        laf_file_close(L);

    return 0;
}

static int read_bytes(lua_State *const L,
                      PHYSFS_file *const file,
                      PHYSFS_uint64 bytes)
{
    struct luaL_Buffer lbuf;
    PHYSFS_sint64 read;

    luaL_buffinit(L, &lbuf);

    do {
        PHYSFS_uint32 to_read = LUAL_BUFFERSIZE;

        if (to_read > bytes)
            to_read = bytes;
        char *buf = luaL_prepbuffer(&lbuf);

        read = PHYSFS_readBytes(file, buf, to_read);

        if (read > 0) {
            luaL_addsize(&lbuf, read);
            bytes = bytes - read;
        }
    } while (read > 0 && bytes > 0);

    luaL_pushresult(&lbuf);
    return 1;
}

static int laf_file_read(lua_State *const L) {
    PHYSFS_file *const *const file = check_laf_open_file(L, 1);

    if (lua_isnumber(L, 2)) {
        const int bytes = lua_tointeger(L, 2);
        luaL_argcheck(L, bytes >= 0, 2, "negative number of bytes");
        if (PHYSFS_eof(*file))
            lua_pushnil(L);
        else
            read_bytes(L, *file, bytes);
        return 1;
    } else {
        static const char *mode_strings[] = {"a", NULL};
        enum modes {
            ALL
        };

        int mode = luaL_checkoption(L, 2, "a", mode_strings);

        if (mode == ALL)
            return read_bytes(L, *file, (PHYSFS_uint64) - 1);
        else
            assert(0);
    }
}

static const luaL_Reg laf_file_lib[] = {
    {"close", laf_file_close},
    {"read",  laf_file_read},
    {"__gc",  laf_file_gc},
    {NULL, NULL},
};

static const luaL_Reg laf_lib[] = {
    {"loadfile", laf_loadfile},
    {"open",     laf_open},
    {NULL, NULL},
};

int luaopen_laf(lua_State *const L) {
     luaL_newmetatable(L, "laf.file");
      lua_pushvalue(L, -1);
     lua_setfield(L, -2, "__index");
     luaL_setfuncs(L, laf_file_lib, 0);
    lua_pop(L, 1);
     luaL_newlib(L, laf_lib);
      lua_newtable(L);
       lua_pushinteger(L, 1);
      lua_setfield(L, -2, "major");
       lua_pushinteger(L, 1);
      lua_setfield(L, -2, "minor");
       lua_pushinteger(L, 1);
      lua_setfield(L, -2, "patch");
     lua_setfield(L, -2, "version");
    return 1;
}
