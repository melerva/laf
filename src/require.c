#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <physfs.h>

extern int laf_loadfile(lua_State *const L);

static void to_filename(lua_State *const L, const int index) {
    const char *module = luaL_checkstring(L, index);

    lua_pushstring(L, "/");
    luaL_gsub(L, module, ".", "/");
    lua_pushstring(L, ".lua");
    lua_concat(L, 3);
}

static int physfs_searcher(lua_State *const L) {
    to_filename(L, 1);
    const char *const filename = lua_tostring(L, 2);

    if (PHYSFS_exists(filename)) {
        lua_pushcfunction(L, laf_loadfile);
        lua_insert(L, 2);
        lua_call(L, 1, 1);
    } else {
        lua_pushfstring(L, "\n\tno physfs file '%s'", filename);
    }
    return 1;
}

void init_physfs_loader(lua_State *const L) {
    /* TODO: rewrite this in C */
    luaL_loadstring(L,
                    "do\n"
                    "  local i = #package.searchers\n"
                    "  while (i > 1) do\n"
                    "    package.searchers[i + 1] = package.searchers[i]\n"
                    "    i = i - 1\n"
                    "  end\n"
                    "end\n"
    );
    lua_call(L, 0, 0);

    lua_getglobal(L, "package");
    lua_getfield(L, -1, "searchers");
    lua_replace(L, -2);
    lua_pushcfunction(L, physfs_searcher);
    lua_seti(L, -2, 2);
    lua_pop(L, 1);
}
