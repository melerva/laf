#include <assert.h>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <malloc.h>
#include <physfs.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int laf_loadfile(lua_State *const L);
extern void init_preloaders(lua_State *const L);
extern void init_physfs_loader(lua_State *const L);

static const char *basename(const char *const filename) {
    const char *last_found = filename;
    const char *sep;

    do {
        sep = strstr(last_found, PHYSFS_getDirSeparator());
        last_found = sep? (sep + 1):last_found;
    } while (sep);

    return last_found;
}

static void deinit_physfs() {
    PHYSFS_deinit();
}

static int msgh(lua_State *const L) {
    const char *msg = lua_tostring(L, 1);
    if (msg == NULL) {
        if (luaL_callmeta(L, 1, "__tostring")
            && (lua_type(L, -1) == LUA_TSTRING))
        {
            return 1;
        } else {
            msg = lua_pushfstring(L, "(error object is a %s value)",
                                  luaL_typename(L, 1));
        }
    }
    luaL_traceback(L, L, msg, 1);
    return 1;
}

static void display_help(FILE *const outfp, const char *const a0) {
    fprintf(outfp,
            "LAF -- Run LAF applications.\n"
            "USAGE:\n"
            "\t%s [--] <application> [args ...]\n"
            "\t%s --help\n",
            a0, a0
    );
}

int main(const int argc, char *const argv[]) {
    if (!PHYSFS_init(argv[0])) {
        fprintf(stderr, "physfs init failed: %s",
                PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode()));
        return EXIT_FAILURE;
    }
    atexit(deinit_physfs);

    bool bundled;

    /* check for bundled application */ {
        /* get executable path */
        const char *const directory = PHYSFS_getBaseDir();
        const char *const executable = basename(argv[0]);
        char *const path = calloc(strlen(directory) + strlen(executable) + 1, sizeof(char));

        strncpy(path, directory, strlen(directory));
        strncat(path, executable, strlen(executable));

        /* try to mount the executable as an archive */
        bundled = !!PHYSFS_mount(path, "/", 0);
        free(path);
    }

    unsigned int app_args_start = 0;

    if (!bundled) {
        char *app_name = NULL;

        if (argc > 1) {
            bool skip_opts = false;

            for (unsigned int i = 1; i < argc; i++) {
                if (!skip_opts) {
                    if (!strcmp(argv[i], "--help")) {
                        display_help(stdout, argv[0]);
                        return EXIT_SUCCESS;
                    }

                    if (!strcmp(argv[i], "--")) {
                        skip_opts = true;
                        continue;
                    }
                }

                app_name = argv[i];
                app_args_start = i;
                break;
            }
        }

        if (app_name == NULL) {
            display_help(stderr, argv[0]);
            return EXIT_FAILURE;
        }

        if (!PHYSFS_mount(app_name, "/", 0)) {
            fprintf(stderr, "%s: error running \"%s\": %s\n",
                    argv[0], app_name,
                    PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode()));
            return EXIT_FAILURE;
        }
    }

    lua_State *const L = luaL_newstate();

    luaL_openlibs(L);

    init_physfs_loader(L);

    lua_getglobal(L, "package");
    lua_newtable(L);
    lua_setfield(L, -2, "nogc");
    lua_pop(L, 1);

    init_preloaders(L);

    /* load arguments into a global 'arg' table */
    lua_newtable(L);
    for (int i = app_args_start; i < argc; i++) {
        lua_pushstring(L, argv[i]);
        lua_rawseti(L, -2, i - app_args_start);
    }
    lua_setglobal(L, "arg");

    /* set up the message handler */ {
        lua_settop(L, 0);
        lua_pushcfunction(L, msgh);
    }

    /* require laf module */
    lua_getglobal(L, "require");
    lua_pushstring(L, "laf");
    lua_call(L, 1, 1);
    lua_setglobal(L, "laf");

    /* load main file */
    lua_pushcfunction(L, laf_loadfile);
    lua_pushstring(L, "init.lua");

    {
        int error = lua_pcall(L, 1, 1, 1);  // load file

        if (!error) {
            error = lua_pcall(L, 0, 0, 1);  // run
        }

        {
            int ret = error;

            switch (error) {
              case LUA_OK:
                ret = EXIT_SUCCESS;
                break;
              case LUA_ERRRUN:
                fprintf(stderr, "%s\n", lua_tostring(L, -1));
                break;
              case LUA_ERRMEM:
                fprintf(stderr, "out of memory\n");
                break;
              case LUA_ERRERR:
                fprintf(stderr, "error errored: %s\n", lua_tostring(L, -1));
                break;
            }

            lua_close(L);
            return ret;
        }
    }
}
