/* register preloaders for statically linked modules */
#include <lua.h>

/* This is how it was done in seed.c
 * I'm not sure why declaring an external function this way works, but it does.
 * TODO: Figure out whether there should be "extern" definitions instead. */
#define REGISTER_LOADER(module_name, loader) {\
    int loader(lua_State *L); \
    lua_pushcfunction(L, loader); \
    lua_setfield(L, -2, module_name); \
}

void init_preloaders(lua_State *const L) {
    lua_getglobal(L, "package");
    lua_getfield(L, -1, "preload");

    REGISTER_LOADER("laf", luaopen_laf);
    REGISTER_LOADER("mod_gc", luaopen_mod_gc);

    lua_pop(L, 2);
}
