# Lua Application Framework

[Lua](https://www.lua.org) is a wonderful language.

It can be run as a script or compiled, it is extremely easy to perform
complex operations, and it is memory-safe!

This is a framework inspired by [LÖVE](https://love2d.org) and built
off of [seed.c](http://stuff.henk.ca/lua/seed.c) to enable tools,
applications, and games to be created easier.

All the low-level stuff can be done in C, then linked to the executable
while all the complicated stuff can be done in Lua, making it less
complicated.

The potential is limitless!

While LÖVE can do everything LAF can do (and more) already, LAF is much
smaller and does not include an entire 2D graphics framework, making it
a better choice for smaller, lighter scripts and command-line tools.
Rather than being optimized for any specific purpose, LAF is extensible
and can be used as a base for any number of libraries, and only includes
the bare necessities (Lua's standard library, and nothing else) by default.

LAF also uses the latest version of Lua (currently 5.4) rather than LuaJIT,
which means the syntax is much more modern.

## How It Works

laf loads data and Lua code from a zip file concatenated to the
executable.
Initially `/init.lua` from the zip file is run, and a loader is added to
`package.searchers` so that `require` searches inside the zip file.
(If a module is not found in the archive, then the default loaders
are used.)

In addition to using `require`, Lua files can be loaded with
`laf.loadfile`, which is like `loadfile` except that it gets the file
from inside the archive.

Basic reading of arbitrary files from the archive is supported.
First use `laf.open`, which is like `io.open` except that only the `"rb"`
option is accepted.
The returned file object implements only `:read`, which is like
`file:read` from `io` but only supporting arguments of either an integer
or the string `'a'`.
`:close` works as normal, and does not need to be called when you can
wait for the garbage collector to do it for you.

TL;DR: It uses PHYSFS for all file I/O.

Binary (i.e. native shared library) modules cannot be loaded from the
archive due to OS limitations, but there are two ways to load them:

 - They can be loaded normally as shared libraries by `require` according
   to `package.cpath` (but your executable won't be self-contained
   anymore)
 - They can be statically linked into the executable, and registered in
   `package.preloaders` by adding them to the `init_preloaders` function.
   (see `src/packages.c`)

## Building

Just run `make`.
